var mongoose = require('mongoose');


var userSchema = mongoose.Schema({
      digital_product:{
        name: String,
        description:String,
        image_name : String,
        quantity:Number,
        price: String,
        isAvaliable:Boolean,
    },
      owner_id: String,
});



module.exports = mongoose.model('DigitalProduct',userSchema);

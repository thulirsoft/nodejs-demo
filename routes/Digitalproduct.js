var express = require('express');
var router = express.Router();
var DigitalProduct = require('./models/digitalproduct.js');
var passport = require('passport');



router.post('/select',function (req,res) {

  DigitalProduct.find({owner_id:req.body.ownerId},function(err,DigitalProducts) {
    if (err) {
      res.send(err);
    }
    console.log(DigitalProducts);

    res.json(DigitalProducts);

  });
});

router.get('/customerProductList',function (req,res) {
  DigitalProduct.find({'digital_product.isAvaliable':true},function (err,DigitalProducts) {
      if (err) {
        res.send(err);
        console.error(err);
      }
      console.log(DigitalProducts);
      res.json(DigitalProducts);
    });

});

router.post('/insert',function(req,res) {

   var newDigitalProduct = new DigitalProduct();
   var productName = req.body.productName;
   var productQuantity = req.body.productQuantity;
   var productPrice = req.body.productPrice;
   var productDescription = req.body.productDescription;
   var productFile = req.body.productFile;
   var isAvaliable = req.body.isAvaliable;
   var owner_id = req.body.ownerId;

   newDigitalProduct.digital_product.name = productName;
   newDigitalProduct.digital_product.description = productDescription;
   newDigitalProduct.digital_product.image_name = productFile;
   newDigitalProduct.digital_product.quantity = productQuantity;
   newDigitalProduct.digital_product.price = productPrice;
   newDigitalProduct.digital_product.isAvaliable = isAvaliable;
   newDigitalProduct.owner_id = owner_id;

   DigitalProduct.findOne({'digital_product.name':productName},function (err,userData) {
if (!userData){

  newDigitalProduct.save(function(err) {
    if (err) {
      res.send(err);
    }
           res.json({error:false,message:'SuccessFully Product added',productName:productName});
         });
         }else{
             res.json({error:true,message:'This Product Already Exist'});
         }
    });


});




module.exports = router;

angular.module('userDataService', [])

	// super simple service
	// each function returns a promise object
	.factory('Todos', ['$http',function($http) {
		return {
			login : function(todoData) {
				return $http.post('/login',todoData);
			},
			create : function(todoData) {
				return $http.post('/signup', todoData);
			},

			addDigitalProduct : function(todoData) {
				return $http.post('/d_product/insert',todoData);
			},
			getDigitalProduct : function(todoData) {
				return $http.post('/d_product/select',todoData);
			},
			dVisibleProduct:function () {
				return $http.get('/d_product/customerProductList');
			}
		}
	}]);

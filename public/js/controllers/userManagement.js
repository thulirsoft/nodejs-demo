angular.module('UserControlls', ['userDataService','ngRoute','jcs-autoValidate','ngCookies'])

.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
  // $locationProvider.html5Mode(true);
 $routeProvider.
    when('/', {
      templateUrl: 'include/home.html',
      controller: 'home'
    })
    .when('/signup', {
      templateUrl: 'include/signup.html',
      controller: 'signup'
    })
    .when('/login', {
      templateUrl: 'include/login.html',
      controller: 'login'
    })
    .when('/shopownerprofile', {
      templateUrl: 'include/shopOwnerProfile.html',
      controller: 'soprofile'
    })
    .when('/logout', {
      templateUrl: 'include/shopOwnerProfile.html',
      controller: 'logout'
    })
    .when('/DigitalProductList', {
      templateUrl: 'include/dProductList.html',
      controller: 'dProductList'
    })
    .when('/addDigitalProduct', {
      templateUrl: 'include/dProduct.html',
      controller: 'dproduct'
    })
    .when('/visibleProducts',{
      templateUrl:'include/dVisibleProduct.html',
      controller:'dVisibleProduct'
    });
}])
.controller('logout',['$cookieStore','$location',function ($cookieStore,$location) {
    $cookieStore.remove('loginId');
    $cookieStore.remove('loginEmail');
    $location.path( "/login" );

}])
.controller('home', ['$scope','$http','Todos', function($scope, $http, Todos) {

}])
.controller('signup', ['$scope','$http','$location','Todos','$cookieStore', function($scope, $http, $location,Todos,$cookieStore) {
    $scope.formData = {};
    $scope.message='';
    $scope.hasMgs = false;

    if($cookieStore.get('loginId')){
      $location.path( "/shopownerprofile" );
    }

    $scope.signup = function() {
      if($scope.signupForm.$valid){
        Todos.create($scope.formData)
          // if successful creation, call our get function to get all the new todos
          .success(function(data) {
              if(data.error){
                $scope.hasMgs = true;
                $scope.message= data.message;
              }else{


                console.log(data);
                $scope.hasMgs = true;
                $scope.message= data.message;
                $location.path( "/login" );
              }

          });
      }
    }
}])
.controller('login', ['$scope','$http','Todos','$cookieStore','$location', function($scope, $http, Todos,$cookieStore,$location) {
  $scope.formData = {};
  $scope.message='';
  $scope.hasMgs = false;

  if($cookieStore.get('loginId')){
    $location.path( "/shopownerprofile" );
  }

  $scope.login = function() {
    console.log($scope.loginForm.$valid);
    if($scope.loginForm.$valid){
      Todos.login($scope.formData)
        // if successful creation, call our get function to get all the new todos
        .success(function(data) {
          console.log(data);
            if(data.error){
              $scope.hasMgs = true;
              $scope.message= data.message;
            }else{

              $cookieStore.put('loginId', data.id);
              $cookieStore.put('loginEmail', data.email);

              console.log($cookieStore.get('loginId'));
              $scope.hasMgs = true;
              $scope.message= data.message;
            $location.path( "/shopownerprofile" );
            }

        });
    }
  }
}])
.controller('soprofile', ['$scope','$http','Todos','$cookieStore','$location', function($scope, $http, Todos,$cookieStore,$location) {
    $scope.id = $cookieStore.get('loginId');
    $scope.email = $cookieStore.get('loginEmail');
    if(!$cookieStore.get('loginId')){
      $location.path( "/login" );
    }
}])
.controller('dproduct', ['$scope','$http','Todos','$cookieStore','$location', function($scope, $http, Todos,$cookieStore,$location) {
  if(!$cookieStore.get('loginId')){
    $location.path( "/login" );
  }

    $scope.formData = {};
    $scope.formData.ownerId = $cookieStore.get('loginId');
    $scope.addDigitalProduct = function() {
      if($scope.addDigitalProductForm.$valid){
        Todos.addDigitalProduct($scope.formData)
        .success(function(data) {
          console.log(data);
            if(data.error){
              $scope.hasMgs = true;
              $scope.message= data.message;
            }else{

              $scope.hasMgs = true;
              $scope.message= data.message;
              $location.path( "/DigitalProductList" );
            }

        });
        console.log($scope.formData);
      }else{
       alert('please Fill all mandatory field');
      }

    }

}])
.controller('dProductList', ['$scope','$http','Todos','$cookieStore','$location', function($scope, $http, Todos,$cookieStore,$location) {
  $scope.userData = {
    ownerId : $cookieStore.get('loginId')
  };
// console.log(angular.toJson({'ownerId':$cookieStore.get('loginId')}));
// console.log($scope.userData);
  Todos.getDigitalProduct($scope.userData)
  .success(function (data) {
    console.log(data);
    	$scope.products = data;
  });
}])
.controller('dVisibleProduct', ['$scope','$http','Todos','$cookieStore','$location', function($scope, $http, Todos,$cookieStore,$location) {
  Todos.dVisibleProduct()
    .success(function (data) {
      $scope.vProducts = data;
    });
}]);
